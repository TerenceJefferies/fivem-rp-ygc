local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_cargocontracts")

RegisterServerEvent("DeliveryComplete")
AddEventHandler("DeliveryComplete", function(pay)
  local source = source
  local user_id = vRP.getUserId({source})
  vRP.giveMoney({user_id, pay})
  vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"United Carriers:",false,"Delivery Completion: ~g~$" .. pay})
end)

RegisterServerEvent("cargocontracts:helpnotification")
AddEventHandler("cargocontracts:helpnotification", function()
  local source = source
  vRPclient.notifyPicture(source,{"CHAR_DEFAULT",3,"Cargo Contract:",false,"If you need to cancel the contract, type /cancelcargo"})
end)

RegisterCommand("cancelcargo", function(source, args, rawCommand)
	if (source > 0) then
    TriggerClientEvent("cargo:cancelcargo", source)
  end
end)
