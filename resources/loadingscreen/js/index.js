$( document ).ready(function() {
  setTimeout(loadDatSkweenie, 2000);
});

function loadDatSkweenie() {
  var banner = ["&nbsp", "Y", "G", "S", "&nbsp", "R", "P", "&nbsp"]
  var rules = ["Always stay in character!",
               "No cop-baiting or randomly killing peds/cops!",
               "If you aren't sure on something, ask in our discord!",
               "New Player? Go to the police station to create an identity ASAP!",
               "Streamers and Youtubers welcomed! Ask for your videos to be shared on our discord!",
               "Any use of script modifications or hacks will result in an instant ban.",
               "Voice chat is mandatory!",
               "Full rules and details are available on Discord."
              ]
  var fadeTime = 500
  var fadeTime2 = 500
  $(".infohold").fadeIn(900)
  $(".steamid").show(900)
  $(banner).each(function( i ) {
    var tCount = Number(i)
    fadeTime = fadeTime + 200
    $(".banner p:nth-child("+tCount+")").hide()
    $( ".banner" ).append( "<p>"+banner[tCount]+"</p>" );
    $(".banner p:nth-child("+tCount+")").fadeIn(fadeTime)
  })
  $(rules).each(function( i ) {
    var rCount = Number(i)
    fadeTime2 = fadeTime2 + 300
    $(".block .text:nth-child("+rCount+")").hide()
    $( ".block:nth-child(1)" ).append( "<p class='text'>"+rules[rCount]+"</p>" )
    $(".block .text:nth-child("+rCount+")").show(fadeTime2)
  })
}
