-- Manifest
resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'
name 'FireScript'
description 'Allows you to simulate fires and smoke.'
author 'Albo1125 (https://www.youtube.com/albo1125)'
version 'v1.4.0'
url 'https://github.com/Albo1125/FireScript'
dependency "vrp"
client_scripts{
  "lib/Tunnel.lua",
  "lib/Proxy.lua",
  'FireScript.net.dll'
}

server_scripts{
  "@vrp/lib/utils.lua",
  'sv_firescript.lua'
}
