local Proxy = module("vrp", "lib/Proxy")

RegisterServerEvent('paycheck:salary')
AddEventHandler('paycheck:salary', function()
  	local user_id = vRP.getUserId(source)
  if vRP.hasPermission(user_id,"taxi.paycheck") then
		vRP.giveBankMoney(user_id,300)
  	vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"PB Taxi Co.",false,"Payday: ~g~$300"})
  end
  if vRP.hasPermission(user_id,"repair.paycheck") then
    vRP.giveBankMoney(user_id,300)
    vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"San Andreas Roadside Assistance",false,"Payday: ~g~$300"})
  end
  if vRP.hasPermission(user_id,"trucker.paycheck") then
    vRP.giveBankMoney(user_id,300)
    vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"United Carriers:",false,"Payday: ~g~$300"})
  end
  if vRP.hasPermission(user_id,"ftd.paycheck") then
    vRP.giveBankMoney(user_id,300)
    vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"Atak-A-Taco Franchises:",false,"Payday: ~g~$300"})
  end
  if vRP.hasPermission(user_id,"security.paycheck") then
    vRP.giveBankMoney(user_id,700)
    vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"Licensed Security:",false,"Payday: ~g~$300"})
  end
  if vRP.hasPermission(user_id,"deputy.paycheck") then
    vRP.giveBankMoney(user_id,1000)
    vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"Sheriff's Dept (Deputy)",false,"Payday: ~g~$1000"})
  end
  if vRP.hasPermission(user_id,"sheriff.paycheck") then
    vRP.giveBankMoney(user_id,2500)
    vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"Sheriff's Dept (Sheriff)",false,"Payday: ~g~$2500"})
  end
  if vRP.hasPermission(user_id,"emsMedic.paycheck") then
    vRP.giveBankMoney(user_id,1200)
    vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"EMS/Fire Rescue",false,"Payday: ~g~$1200"})
  end
  if vRP.hasPermission(user_id,"emsChief.paycheck") then
    vRP.giveBankMoney(user_id,2500)
    vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"EMS/Fire Rescue Chief",false,"Payday: ~g~$2500"})
  end

  --OLD paychecks

	if vRP.hasPermission(user_id,"bankdriver.paycheck") then
		vRP.giveBankMoney(user_id,2000)
		vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"Bank Driver",false,"Payday: ~g~$2000"})
	end
	if vRP.hasPermission(user_id,"pilot.paycheck") then
		vRP.giveBankMoney(user_id,2000)
		vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"Airline Company",false,"Payday: ~g~$2000"})
	end
	if vRP.hasPermission(user_id,"ups.paycheck") then
		vRP.giveBankMoney(user_id,2000)
		vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"UPS Company",false,"Payday: ~g~$2000"})
	end
	if vRP.hasPermission(user_id,"air.paycheck") then
		vRP.giveBankMoney(user_id,2000)
		vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"Airline Company",false,"Payday: ~g~$2000"})
	end
	if vRP.hasPermission(user_id,"trash.paycheck") then
		vRP.giveBankMoney(user_id,2000)
		vRPclient.notifyPicture(source,{"CHAR_BANK_MAZE",1,"Trash Collector",false,"Payday: ~g~$2000"})
	end
end)

RegisterServerEvent('paycheck:bonus')
AddEventHandler('paycheck:bonus', function()
  	local user_id = vRP.getUserId(source)
	if vRP.hasPermission(user_id,"user.paycheck") then
		vRP.giveBankMoney(user_id,50)
		vRPclient.notify(source,{"State Welfare: ~g~$50"})
	end
	if vRP.hasPermission(user_id,"job.taxable.low") then
		vRP.tryFullPayment(user_id,100)
		vRPclient.notify(source,{"Tax Dept: You are on the LOW tax band. -$100"})
	end
  if vRP.hasPermission(user_id,"job.taxable.mid") then
    vRP.tryFullPayment(user_id,250)
    vRPclient.notify(source,{"Tax Dept: You are on the MID tax band. -$250"})
  end
  if vRP.hasPermission(user_id,"job.taxable.high") then
    vRP.tryFullPayment(user_id,500)
    vRPclient.notify(source,{"Tax Dept: You are on the HIGH tax band. -$500"})
  end
end)
